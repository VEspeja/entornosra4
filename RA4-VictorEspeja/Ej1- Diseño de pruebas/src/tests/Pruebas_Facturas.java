package tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

public class Pruebas_Facturas {
	static Factura facturaPrueba;
	static GestorContabilidad gestorPruebas;
	static GestorContabilidad gestorPruebas1;
	static GestorContabilidad gestorPruebasComprobar;
	static Cliente unCliente;
	static Factura unaFactura;
	
	@BeforeAll
	public static void prepararClasePruebas() {
		facturaPrueba = new Factura();
		gestorPruebas = new GestorContabilidad();
		gestorPruebas1 = new GestorContabilidad();
		gestorPruebasComprobar = new GestorContabilidad();
		unCliente = new Cliente("1234V","Victor",LocalDate.now());
		unaFactura = new Factura("12345",LocalDate.now(),"Patatas",5,2,unCliente);
	}
	/**
	 * esta prueba se encarga de calcular el precio de la factura con un articulo
	 */
	@Test
	public void testCalcularPrecioProducto1() {
		facturaPrueba.setCantidad(3);
		facturaPrueba.setPrecioUnidad(2.5F);
		
		float esperado= 7.5F;
		float actual=facturaPrueba.calcularPrecioTotal(gestorPruebas.getListaFacturas());
		
		assertEquals(esperado, actual,0.0F);
	}
	/**
	 * esta prueba se encarga de buscar una factura inexistente en una lista que contiene facturas
	 */
	@Test
	public void testBuscarFacturaInxistenteConFacturas() {
		gestorPruebas.getListaFacturas().add(unaFactura);
		Factura actual=gestorPruebas.buscarFactura("74535");
		assertNull(actual);
	}
	/**
	 * esta prueba se encarga de buscar una factura que si existe en una lista que contiene facturas
	 */
	@Test
	public void testBuscarFacturaExistenteConFacturas() {

		Factura actual=gestorPruebas.buscarFactura("12345");
		assertSame(unaFactura, actual);
	}
	/**
	 * esta prueba se encarga de crear una factura inexistente en una lista que contiene facturas
	 */
	@Test
	public void crearFacturaInexistenteConFacturas() {
		Factura nuevaFactura= new Factura ("147258",LocalDate.now(),"Pa�uelos",1,1,unCliente);
		gestorPruebas1.crearFactura(nuevaFactura);
		boolean actual=gestorPruebas1.getListaFacturas().contains(nuevaFactura);
		assertTrue(actual);
	}
	/**
	 * esta prueba se encarga de crear una factura que ya esta creada en una lista que contiene facturas
	 */
	@Test
	public void crearFacturaExistente() {
		Factura nuevaFactura= new Factura ("147258",LocalDate.now(),"Jabon",2,6,unCliente);
		gestorPruebas1.crearFactura(nuevaFactura);
		boolean actual=gestorPruebas1.getListaFacturas().contains(nuevaFactura);
		assertFalse(actual);
	}
	/**
	 * esta prueba se encarga comprobar cual es la factura mas cara de las 2 que hay
	 */
	@Test 
	public void comprobarFacturaMasCaraCon2Facturas() {
		Factura nuevaFactura= new Factura ("147258",LocalDate.now(),"Pa�uelos",1,1,unCliente);
		Factura otraFactura= new Factura ("147258",LocalDate.now(),"Jabon",2,6,unCliente);
		gestorPruebasComprobar.getListaFacturas().add(nuevaFactura);
		gestorPruebasComprobar.getListaFacturas().add(otraFactura);
		Factura actual=gestorPruebasComprobar.facturaMasCara();
		
		assertEquals(otraFactura, actual);
	}
	/**
	 * esta prueba se encarga de calcular la facturacion anual de un a�o que ha sido pasado
	 */
	@Test
	public void calcularFacturacionAnualConProductos() {
		gestorPruebas.getListaFacturas().clear();
		gestorPruebas.getListaFacturas().add(unaFactura);
		float actual= gestorPruebas.calcularFacturacionAnual(2018);
		assertEquals(10, actual);
	}
	
}
