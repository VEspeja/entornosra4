package tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

class Pruebas_Eliminar {
	static GestorContabilidad gestorPruebas;
	static GestorContabilidad gestorPruebas1;
	static Cliente unCliente;
	static Factura unaFactura;
	/**
	 * se prepara la clase con los arrays y objetos a usar
	 */
	@BeforeAll
	public static void prepararClasePruebas() {
		gestorPruebas = new GestorContabilidad();
		gestorPruebas1 = new GestorContabilidad();
		unCliente = new Cliente("1234V","Victor",LocalDate.now());
		unaFactura = new Factura("12345",LocalDate.now(),"Patatas",5,2,unCliente);
	}
	/**
	 * esta prueba se encarga de comprobar si se ha eliminado una factura que existe en el array
	 */
	@Test
	public void eliminarFacturaExistente() {
		gestorPruebas.getListaFacturas().clear();
		gestorPruebas.getListaFacturas().add(unaFactura);
		gestorPruebas.eliminarFactura("12345");
		boolean actual= gestorPruebas.getListaFacturas().contains(unaFactura);
		
		assertFalse(actual);
	}
	/**
	 * esta prueba se encarga de comprobar si se ha eliminado una factura que no se encontraba en el array
	 */
	@Test
	public void eliminarFacturaInexistente() {
		boolean actual = gestorPruebas1.getListaFacturas().contains(unaFactura);
		assertFalse(actual);
	}
	/**
	 * esta prueba se encarga de comprobar si se ha eliminado un cliente que existe en el array
	 */
	@Test 
	public void eliminarClienteExistente() {
		gestorPruebas.getListaClientes().clear();
		gestorPruebas.getListaClientes().add(unCliente);
		gestorPruebas.eliminarCliente("1234V");
		boolean encontrado=gestorPruebas.getListaClientes().contains(unCliente);
		assertFalse(encontrado);
	}
	/**
	 * esta prueba se encarga de comprobar si se ha eliminado un cliente que no existe en el array
	 */
	@Test 
	public void eliminarClienteInexistente() {
		boolean encontrado=gestorPruebas1.getListaClientes().contains(unCliente);
		assertFalse(encontrado);
	}
}
