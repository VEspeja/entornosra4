package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.GestorContabilidad;

class Pruebas_SinClientes {
	static GestorContabilidad gestorPruebas;
	static GestorContabilidad gestorPruebas1;
	static GestorContabilidad gestorPruebasAlta;
	static Cliente unCliente;
	
	@BeforeAll
	public static void prepararClasePruebas() {
		gestorPruebas = new GestorContabilidad();
		gestorPruebas1 = new GestorContabilidad();
		gestorPruebasAlta = new GestorContabilidad();

	}
	/**
	 * esta prueba se encarga de buscar un cliente que no existe en una lista vacia
	 */
	@Test 
	public void testBuscarClienteInexistenteSinClientes() {
		String dni="2345234";
		
		Cliente actual= gestorPruebas.buscarCliente(dni);
		assertNull(actual);
	}
	/**
	 * esta prueba se encarga de añadir un cliente que no existe a una lista que esta vacia
	 */
	@Test
	public void testAñadirClienteInexistenteSinClientes() {
		Cliente nuevoCliente=new Cliente("17289M","Marcos",LocalDate.now());
		gestorPruebas1.altaCliente(nuevoCliente);
		boolean actual=gestorPruebas1.getListaClientes().contains(nuevoCliente);
		
		assertTrue(actual);
	}
	/**
	 * esta prueba se encarga de comprobar cual es cliente mas antiguo de una lista que esta vacia
	 */
	@Test 
	public void testClienteMasAntiguoSinClientes() {
		Cliente actual=gestorPruebasAlta.clienteMasAntiguo();
		assertNull(actual);
	}

}
