package tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

class Pruebas_contarFacturas {

	static GestorContabilidad gestorPruebas;
	static Cliente unCliente;
	static Factura unaFactura;
	/**
	 * se prepara la clase con lo que se va a usar
	 */
	@BeforeAll
	public static void prepararClasePruebas() {
		gestorPruebas = new GestorContabilidad();
		unCliente = new Cliente("1234V","Victor",LocalDate.now());
		unaFactura = new Factura("12345",LocalDate.now(),"Patatas",5,2,unCliente);
	}
	/**
	 *  esta prueba se encarga de comprobar que el metodo devuelve 1 factura
	 */
	@Test 
	public void contarFacturasExistentesCon1Factura() {
		gestorPruebas.getListaFacturas().add(unaFactura);
		int actual = gestorPruebas.cantidadFacturasPorCliente(unCliente.getDni());
		int esperado=1;
		assertEquals(esperado, actual);
	}
	/**
	 *  esta prueba se encarga de comprobar que el metodo devuelve 2 facturas
	 */
	@Test 
	public void contarFacturasExistentesCon2Factura() {
		Factura otraFactura= new Factura("45678",LocalDate.now(),"Jabon",3,1,unCliente);
		gestorPruebas.getListaFacturas().clear();
		gestorPruebas.getListaFacturas().add(unaFactura);
		gestorPruebas.getListaFacturas().add(otraFactura);
		int actual = gestorPruebas.cantidadFacturasPorCliente(unCliente.getDni());
		int esperado=2;
		assertEquals(esperado, actual);
	}
	/**
	 * esta prueba se encarga de comprobar que no devuelve ninguna factura en una lista vacia
	 */
	@Test 
	public void contarFacturasSinFacturas() {
		gestorPruebas.getListaFacturas().clear();
		int actual = gestorPruebas.cantidadFacturasPorCliente(unCliente.getDni());
		int esperado=0;
		assertEquals(esperado, actual);
	}
}
