package tests;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

class Test_AsignarClienteAFactura {

	static GestorContabilidad gestorPruebas;
	static Cliente unCliente;
	static Factura unaFactura;
	/**
	 * se prepara la clase con lo que se va a usar
	 */
	@BeforeAll
	public static void prepararClasePruebas() {
		gestorPruebas = new GestorContabilidad();
		unCliente = new Cliente("1234V","Victor",LocalDate.now());
		unaFactura = new Factura("12345",LocalDate.now(),"Patatas",5,2);
	}
	/**
	 * esta prueba comprueba si se ha añadido el cliente a la factura
	 */
	@Test
	public void añadirClienteAFactura() {
	gestorPruebas.asignarClienteAFactura(unCliente.getDni(), unaFactura.getCodigoFactura());
	Factura facturaActual= gestorPruebas.buscarFactura(unaFactura.getCodigoFactura());
	boolean actual=facturaActual.getCliente().equals(unCliente);
	assertTrue(actual);
	}
	/**
	 * esta prueba se encarga de comprobar si se ha añadido un cliente que no existe a la factura que si existe 
	 */
	@Test
	public void añadirClienteInexistenteAFactura() {
	gestorPruebas.getListaFacturas().clear();
	gestorPruebas.asignarClienteAFactura("123", unaFactura.getCodigoFactura());
	Factura facturaActual= gestorPruebas.buscarFactura(unaFactura.getCodigoFactura());
	boolean actual=facturaActual.getCliente().equals(unCliente);
	assertFalse(actual);
	}
}
