package tests;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

class Pruebas_SinFacturas {
	static GestorContabilidad gestorPruebas;
	static GestorContabilidad gestorPruebas1;
	static GestorContabilidad gestorPruebasComprobar;

	static Cliente unCliente;
	
	@BeforeAll
	public static void prepararClasePruebas() {
		gestorPruebas = new GestorContabilidad();
		gestorPruebas1 = new GestorContabilidad();
		gestorPruebasComprobar = new GestorContabilidad();
		unCliente = new Cliente("1234V","Victor",LocalDate.now());
	}	
	/**
	 * esta prueba se encarga de buscar una factura que no existe en una lista vacia	
	 */
	@Test 
	public void testBuscarFacturaInexistenteSinFacturas() {

		String codigo= "23145";
		Factura actual= gestorPruebas.buscarFactura(codigo);
		assertNull(actual);
	}
	/**
	 * esta prueba se encarga de crear una factura que no existe en una lista vacia	
	 */
	@Test
	public void crearFacturaInexistenteSinFacturas() {
		Factura nuevaFactura= new Factura ("456789",LocalDate.now(),"Aceite",3,4,unCliente);
		gestorPruebas1.crearFactura(nuevaFactura);
		boolean actual=gestorPruebas1.getListaFacturas().contains(nuevaFactura);
		assertTrue(actual);
	}
	/**
	 * esta prueba se encarga de comprobar que factura es la mas cara en una lista vacia	
	 */
	@Test 
	public void comprobarFacturaMasCaraSinFacturas() {
		Factura actual=gestorPruebasComprobar.facturaMasCara();
		assertNull(actual);
	}
	/**
	 * esta prueba se encarga de calcular la facturacion anual de una lista que esta vacia
	 */
	@Test 
	public void calcularFacturacionAnualSinFacturas() {
		float actual = gestorPruebas.calcularFacturacionAnual(2017);
		assertEquals(0, actual);
	}
	
}
