package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.BeforeClass;

import static java.lang.Math.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

class Pruebas_Clientes {
	
	static GestorContabilidad gestorPruebas;
	static GestorContabilidad gestorPruebas1;
	static GestorContabilidad gestorPruebasComprobar;
	static Cliente unCliente;
	
	@BeforeAll
	public static void prepararClasePruebas() {
		gestorPruebas = new GestorContabilidad();
		gestorPruebas1 = new GestorContabilidad();
		gestorPruebasComprobar = new GestorContabilidad();
		unCliente = new Cliente("1234V","Victor",LocalDate.now());
	}
	/**
	 * esta prueba se encarga de buscar un cliente que no existe en un array que contiene clientes
	 */
	@Test
	public void testBuscarClienteInxistenteConClientes() {
		gestorPruebas.getListaClientes().add(unCliente);
		String dni="12V";
		Cliente actual= gestorPruebas.buscarCliente(dni);
		assertNull(actual);
	}
	/**
	 * esta prueba se encarga de buscar un cliente que si existe en un array que contiene clientes
	 */
	@Test
	public void testBuscarClienteExistenteConClientes() {
		Cliente actual= gestorPruebas.buscarCliente("1234V");
		assertSame(unCliente, actual);
	}
	/**
	 * esta prueba se encarga de añadir un cliente que no existe en un array que contiene clientes
	 */
	@Test
	public void testAñadirClienteInexistenteConClientes() {
		Cliente nuevoCliente=new Cliente("34910J","Jesus",LocalDate.now());
		gestorPruebas1.altaCliente(nuevoCliente);
		boolean actual=gestorPruebas1.getListaClientes().contains(nuevoCliente);
		
		assertTrue(actual);
	}
	/**
	 * esta prueba se encarga de añadir a la lista un cliente que si existe en la lista
	 * en la que se encuentra un cliente con el mismo dni
	 */
	@Test
	public void testAñadirClienteExistente() {
		Cliente nuevoCliente = new Cliente ("34910J","Paco",LocalDate.now());
		gestorPruebas1.altaCliente(nuevoCliente);
		boolean actual=gestorPruebas1.getListaClientes().contains(nuevoCliente);
		
		assertFalse(actual);
	}
	/**
	 * esta prueba se encarga de comprobar cual es el cliente mas antiguo de 2 clientes que hay en la lista
	 */
	@Test
	public void testClienteMasAntiguoCon2Clientes() {
		Cliente nuevoCliente=new Cliente("34910J","Jesus",LocalDate.of(2005, 03, 18));
		Cliente nuevoCliente1=new Cliente("17289M","Marcos",LocalDate.of(1999, 9, 01));
		gestorPruebasComprobar.getListaClientes().add(nuevoCliente);
		gestorPruebasComprobar.getListaClientes().add(nuevoCliente1);
		Cliente actual=gestorPruebasComprobar.clienteMasAntiguo();
		
		assertEquals(nuevoCliente1, actual);
	}
	/**
	 * Este test comprueba el cliente mas antiguo 
	 * con 3 clientes añadiendo uno de edad intermedia
	 */
	@Test 
	public void testClienteMasAntiguoCon3Clientes() {
		Cliente nuevoCliente=new Cliente("31743V","Victor",LocalDate.of(2002, 8, 23));
		gestorPruebasComprobar.getListaClientes().add(nuevoCliente);
		Cliente actual=gestorPruebasComprobar.clienteMasAntiguo();
		Cliente esperado=gestorPruebasComprobar.buscarCliente("17289M");
		
		assertEquals(esperado, actual);
	}
	
}
