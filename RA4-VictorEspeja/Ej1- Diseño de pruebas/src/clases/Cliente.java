package clases;

import java.time.LocalDate;

public class Cliente {
	private String nombre;
	private String dni;
	private LocalDate fechaAlta;
	
	public Cliente( String dni, String nombre, LocalDate fechaAlta) {
		this.dni = dni;
		this.nombre = nombre;
		this.fechaAlta = fechaAlta;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public LocalDate getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(LocalDate fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	@Override
	public String toString() {
		return "Cliente [nombre=" + nombre + ", dni=" + dni + ", fechaAlta=" + fechaAlta + "]";
	}
	
	
	
}
