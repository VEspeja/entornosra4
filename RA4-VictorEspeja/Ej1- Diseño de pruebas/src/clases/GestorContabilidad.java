package clases;

import java.time.LocalDate;
import java.util.ArrayList;

public class GestorContabilidad {
	private ArrayList<Factura> listaFacturas;
	private ArrayList<Cliente> listaClientes;
	
	public GestorContabilidad() {
		listaFacturas = new ArrayList<Factura>();
		listaClientes = new ArrayList<Cliente>();
	}
	
	// metodo temporal para facilitar las pruebas
	public ArrayList<Factura> getListaFacturas() {
		return listaFacturas;
	}
	
	// metodo temporal para facilitar las pruebas
	
	public ArrayList<Cliente> getListaClientes() {
		return listaClientes;
	}
	
	public Cliente buscarCliente(String dni) {
		String dniABuscar= dni;
		for(int i=0;i<listaClientes.size();i++) {
			if(dniABuscar.equalsIgnoreCase(listaClientes.get(i).getDni())) {
				return listaClientes.get(i);
			}
			else {
				return null;
			}
		}
		return null;
		
	}


	public Factura buscarFactura (String codigo) {
		String codigoABuscar= codigo;
		for(int i=0;i<listaFacturas.size();i++) {
			if(codigo.equalsIgnoreCase(listaFacturas.get(i).getCodigoFactura())) {
				return listaFacturas.get(i);
			}
			else {
				return null;
			}
		}
		return null;
	}
	
	public void altaCliente (Cliente cliente) {
		if(buscarCliente(cliente.getDni()) == null) {
			listaClientes.add(cliente);
		}
		
	}
	
	public void crearFactura(Factura factura) {
		boolean repetido=false;
		for(int i=0;i<listaFacturas.size();i++) {
			if(listaFacturas.get(i).getCodigoFactura()==factura.getCodigoFactura()) {
				repetido= true;
			}
		}
		if(repetido==false) {
			listaFacturas.add(factura);
		
		}
	}

	public Cliente clienteMasAntiguo() {
		LocalDate max=LocalDate.now();
		Cliente antiguo = null;
		for(int i=0;i<listaClientes.size();i++) {
			if(listaClientes.get(i).getFechaAlta().isBefore(max)) {
				max=listaClientes.get(i).getFechaAlta();
				antiguo=listaClientes.get(i);
			}
			else {
				return null;
			}
			
		}
		return antiguo;
		
	}

	public Factura facturaMasCara() {
		float facturaMaxima=0;
		Factura maxima=null;
		for(int i=0;i<listaFacturas.size();i++) {
			if((listaFacturas.get(i).getCantidad()*listaFacturas.get(i).getPrecioUnidad())
					>facturaMaxima) {
				facturaMaxima=listaFacturas.get(i).getCantidad()*listaFacturas.get(i).getPrecioUnidad();
				maxima=listaFacturas.get(i);
			}
			else {
				return null;
			}
		}
		return maxima;
	}
	
	public float calcularFacturacionAnual(int anno) {
		return 0;
		
	}

	public void asignarClienteAFactura(String dni, String codigoFactura) {
		
	}
	public void eliminarFactura(String codigo) {
		
	}

	public void eliminarCliente(String dni) {
		
	}
	public int cantidadFacturasPorCliente(String dni) {
		return 0;
	}
	
}
