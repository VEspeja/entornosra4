package clases;
import java.time.LocalDate;
import java.util.ArrayList;

public class Factura {
	private String codigoFactura;
	private LocalDate fecha;
	private String nombreProducto;
	private float precioUnidad;
	private int cantidad;
	private Cliente cliente;
	
	public Factura(String codigoFactura, LocalDate fecha, String nombreProducto, float precioUnidad, int cantidad,
			Cliente cliente) {

		this.codigoFactura = codigoFactura;
		this.fecha = fecha;
		this.nombreProducto = nombreProducto;
		this.precioUnidad = precioUnidad;
		this.cantidad = cantidad;
		this.cliente = cliente;
	}
	
	public Factura() {
		this.codigoFactura = "";
		this.fecha = null;
		this.nombreProducto = "";
		this.precioUnidad = 0;
		this.cantidad = 0;
		this.cliente = null;
	}



	public Factura(String string, LocalDate now, String string2, int i, int j) {
		// TODO Auto-generated constructor stub
	}


	public String getCodigoFactura() {
		return codigoFactura;
	}

	public void setCodigoFactura(String coidgoFactura) {
		this.codigoFactura = coidgoFactura;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public String getNombreProducto() {
		return nombreProducto;
	}

	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

	public float getPrecioUnidad() {
		return precioUnidad;
	}

	public void setPrecioUnidad(float precioUnidad) {
		this.precioUnidad = precioUnidad;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public float calcularPrecioTotal(ArrayList<Factura> listaFacturas) {
		float precioFactura=0;
			for(int i=0;i<listaFacturas.size();i++) {
				precioFactura= precioFactura+(cantidad*precioUnidad);
			}
		return precioFactura;
	}
}
